## [1.13.9](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.8...v1.13.9) (2024-07-05)


### Bug Fixes

* confirm on right 2nd comitt ([9536e72](https://gitlab.com/packages2855812/wpmvue-starter/commit/9536e72105256f5a18f70929f1f34b86a8af696e))

## [1.13.8](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.7...v1.13.8) (2024-07-04)


### Bug Fixes

* clear_errors bug ([3ae65f7](https://gitlab.com/packages2855812/wpmvue-starter/commit/3ae65f7e8882a841564eb1c7ebe9c2ea455889af))

## [1.13.7](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.6...v1.13.7) (2024-06-14)


### Bug Fixes

* scrollPercentage v2 ([1a132eb](https://gitlab.com/packages2855812/wpmvue-starter/commit/1a132eb4691edddb1280243d7085bd53cf6c356e))

## [1.13.6](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.5...v1.13.6) (2024-06-14)


### Bug Fixes

* scrollPercentage ([ed8c35e](https://gitlab.com/packages2855812/wpmvue-starter/commit/ed8c35e3b4193dc536ce52c9fb8d931122594548))

## [1.13.5](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.4...v1.13.5) (2024-05-27)


### Bug Fixes

* removed error.js ([792d22c](https://gitlab.com/packages2855812/wpmvue-starter/commit/792d22cd92b52c0f71efbec8839f1e5eac6ad4ad))

## [1.13.4](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.3...v1.13.4) (2024-05-27)


### Bug Fixes

* duplicated error names ([1bc39ec](https://gitlab.com/packages2855812/wpmvue-starter/commit/1bc39ec35dc4080dc73a19d6257ef5a9241b155c))

## [1.13.3](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.2...v1.13.3) (2024-05-23)


### Bug Fixes

* get year added ([21b7a33](https://gitlab.com/packages2855812/wpmvue-starter/commit/21b7a3357121c8dda3daa8ba33ad1592a0fdce8a))
* get year added 2 ([1af39aa](https://gitlab.com/packages2855812/wpmvue-starter/commit/1af39aa83c3b51f4cf90c96575c11502e41c80de))

## [1.13.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.1...v1.13.2) (2024-05-23)


### Bug Fixes

* get year added ([3cb21c9](https://gitlab.com/packages2855812/wpmvue-starter/commit/3cb21c979bc79005dbf7cf766f8539e2e2bb9ee8))

## [1.13.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.13.0...v1.13.1) (2024-05-17)


### Bug Fixes

* toggle fix ([0ee81a5](https://gitlab.com/packages2855812/wpmvue-starter/commit/0ee81a530d435b12ebb16da3e56232afd3352f61))

# [1.13.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.12.0...v1.13.0) (2024-05-16)


### Bug Fixes

* export section ([9a48cef](https://gitlab.com/packages2855812/wpmvue-starter/commit/9a48cef5be7f5977845b4ae6de24857458cda935))


### Features

* updated scaff to blagajna ([61c5d2b](https://gitlab.com/packages2855812/wpmvue-starter/commit/61c5d2b97d7bc44b6e97fe27a583b9e04f395248))

# [1.12.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.11.2...v1.12.0) (2024-05-16)


### Features

* updated scaff to blagajna ([c03185f](https://gitlab.com/packages2855812/wpmvue-starter/commit/c03185feb081b471efe6b9a6881ee2221045b8f5))

## [1.11.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.11.1...v1.11.2) (2024-05-16)


### Bug Fixes

* package json update ([d6059d7](https://gitlab.com/packages2855812/wpmvue-starter/commit/d6059d77ea814a41b4ad6526090d18ae5479d881))
* package json update2 ([16ef865](https://gitlab.com/packages2855812/wpmvue-starter/commit/16ef865ff3605d02b16905ed7f9786f58f3e44c1))

## [1.11.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.11.0...v1.11.1) (2024-05-16)


### Bug Fixes

* package json update ([b917225](https://gitlab.com/packages2855812/wpmvue-starter/commit/b917225f98c2b6234e478cd338c01a78c292ce64))

# [1.11.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.10.1...v1.11.0) (2024-05-16)


### Bug Fixes

* fs-extra dependency ([0ae4f05](https://gitlab.com/packages2855812/wpmvue-starter/commit/0ae4f05e2ccb1e171ba60724a51e02de2179b41d))
* fs-extra dependency ([9f5e5b6](https://gitlab.com/packages2855812/wpmvue-starter/commit/9f5e5b628ba7b19eb8e17ec6c40689498ece7cec))


### Features

* DragDrop + functionality ([adf8139](https://gitlab.com/packages2855812/wpmvue-starter/commit/adf81391cd26d3c701da9b7a312e80acaf7829e3))
* focus event in Input ([edf925e](https://gitlab.com/packages2855812/wpmvue-starter/commit/edf925eeb21bec3a1d3b08b7e3ab7e7501f577c1))

## [1.10.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.10.0...v1.10.1) (2024-04-26)


### Bug Fixes

* DragDrop open PDF in new tab ([4b3b369](https://gitlab.com/packages2855812/wpmvue-starter/commit/4b3b3692f67469a5ad6f3947e9d71380623b3f4b))

# [1.10.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.5...v1.10.0) (2024-04-24)


### Features

* DragDrop + functionality ([17cf869](https://gitlab.com/packages2855812/wpmvue-starter/commit/17cf869c283002e48cfdfce2765cf8a9f0974383))

## [1.9.5](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.4...v1.9.5) (2024-04-24)


### Bug Fixes

* storage functions added ([bc0a08e](https://gitlab.com/packages2855812/wpmvue-starter/commit/bc0a08e70a495c54995515ae7f15e33779eee546))

## [1.9.4](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.3...v1.9.4) (2024-04-23)


### Bug Fixes

* disabled button link ([3f5071f](https://gitlab.com/packages2855812/wpmvue-starter/commit/3f5071f4a3711f1951591f3ca4326da32d8a886b))

## [1.9.3](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.2...v1.9.3) (2024-04-22)


### Bug Fixes

* Endings problem ([1a13154](https://gitlab.com/packages2855812/wpmvue-starter/commit/1a13154e990430ce1eba65f85c2df69506793da3))

## [1.9.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.1...v1.9.2) (2024-04-16)


### Bug Fixes

* TopTabs - asRoute ([49d183a](https://gitlab.com/packages2855812/wpmvue-starter/commit/49d183a6c664c9b153d4e2476095a90baada3d60))

## [1.9.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.9.0...v1.9.1) (2024-04-16)


### Bug Fixes

* Toggle - typeof null == "object" bug ([c45d8b5](https://gitlab.com/packages2855812/wpmvue-starter/commit/c45d8b57c45b2d7f4c7d9f86aabb75bbf6cfcef6))

# [1.9.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.8.0...v1.9.0) (2024-04-11)


### Features

* row class callback in Table ([59011d9](https://gitlab.com/packages2855812/wpmvue-starter/commit/59011d95eacc0ab8e29ae1de467207af5146f969))

# [1.8.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.14...v1.8.0) (2024-04-08)


### Features

* Table clickable row ([23ddaae](https://gitlab.com/packages2855812/wpmvue-starter/commit/23ddaaee1cdfa7cf8db13e5dd52ef90b057fefdd))

## [1.7.14](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.13...v1.7.14) (2024-04-05)


### Bug Fixes

* Endings problem ([3fd588b](https://gitlab.com/packages2855812/wpmvue-starter/commit/3fd588bf65bf024d84ac97b278cac63122d3e211))

## [1.7.13](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.12...v1.7.13) (2024-04-05)


### Bug Fixes

* Endings problem ([b8687e7](https://gitlab.com/packages2855812/wpmvue-starter/commit/b8687e765446b8c749af86b807e2922564ae9ec8))

## [1.7.12](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.11...v1.7.12) (2024-04-05)


### Bug Fixes

* Carbon Constants exported problem ([363bb8c](https://gitlab.com/packages2855812/wpmvue-starter/commit/363bb8c3b3711c44b1d57e23dde85e02a06bef8e))

## [1.7.11](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.10...v1.7.11) (2024-04-05)


### Bug Fixes

* Carbon Constants exported ([df5dedc](https://gitlab.com/packages2855812/wpmvue-starter/commit/df5dedca70ff308405f3ad8079800429323040cc))

## [1.7.10](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.9...v1.7.10) (2024-04-05)


### Bug Fixes

* ClickDirective, popup component, Input-change debounced ([6ae3aa3](https://gitlab.com/packages2855812/wpmvue-starter/commit/6ae3aa3e88092f6ae88da31f2fb7977cd5fd0ded))

## [1.7.9](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.8...v1.7.9) (2024-04-02)


### Bug Fixes

* Apply middleware subsequent middlewares, notifications duration option ([f812105](https://gitlab.com/packages2855812/wpmvue-starter/commit/f8121058aa829243d6c5ab3a712bb5ade61cebac))

## [1.7.8](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.7...v1.7.8) (2024-03-29)


### Bug Fixes

* Searchable feature with search() function ([9560453](https://gitlab.com/packages2855812/wpmvue-starter/commit/9560453d777f908b79b3083f416f11d62e59e3a5))

## [1.7.7](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.6...v1.7.7) (2024-03-27)


### Bug Fixes

* Searchable feature with search() function ([e1d8bb9](https://gitlab.com/packages2855812/wpmvue-starter/commit/e1d8bb924b3e59f81f4c88986e763d24f37a8cd9))

## [1.7.6](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.5...v1.7.6) (2024-03-27)


### Bug Fixes

* Table bottom emit ([9dd6283](https://gitlab.com/packages2855812/wpmvue-starter/commit/9dd62832f4a94f5bedfc7ab6e08c0f5aea528876))

## [1.7.5](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.4...v1.7.5) (2024-03-27)


### Bug Fixes

* Toggle added, button link functionality added, ([5c6610f](https://gitlab.com/packages2855812/wpmvue-starter/commit/5c6610f87d82a57bacf88b960a836ea6587773c4))

## [1.7.4](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.3...v1.7.4) (2024-03-26)


### Bug Fixes

* Table "ni rezultatov" ([5d8962d](https://gitlab.com/packages2855812/wpmvue-starter/commit/5d8962d4fe52401232eb1273a91acb6330c699a3))

## [1.7.3](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.2...v1.7.3) (2024-03-26)


### Bug Fixes

* Table header ([ff6c069](https://gitlab.com/packages2855812/wpmvue-starter/commit/ff6c0692233f60e33da202d5c2fa1c72ac1f9193))

## [1.7.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.1...v1.7.2) (2024-03-26)


### Bug Fixes

* Button as router-link styles ([569010d](https://gitlab.com/packages2855812/wpmvue-starter/commit/569010d5ea68010b05876b72326fcad5887ca138))

## [1.7.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.7.0...v1.7.1) (2024-03-26)


### Bug Fixes

* Scrollable table ([86cff3f](https://gitlab.com/packages2855812/wpmvue-starter/commit/86cff3f34a30253f1da4b48dad30ca3aca37cea0))
* Toggle added, button link functionality added, ([f8ad0f6](https://gitlab.com/packages2855812/wpmvue-starter/commit/f8ad0f67f62856c7a22e66d9787386090ed301cc))

# [1.7.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.6.2...v1.7.0) (2024-03-25)


### Bug Fixes

* ModalWindow export ([c207d1b](https://gitlab.com/packages2855812/wpmvue-starter/commit/c207d1b0597fc107616210cfcb208618216fd349))


### Features

* Confirm plugin ([0f4f20e](https://gitlab.com/packages2855812/wpmvue-starter/commit/0f4f20e6212c288b7d25fa2f003161bd8059e8ac))

## [1.6.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.6.1...v1.6.2) (2024-03-25)


### Bug Fixes

* [@bottom](https://gitlab.com/bottom) event in Scroll ([52b28cc](https://gitlab.com/packages2855812/wpmvue-starter/commit/52b28ccd24a1ebe4dc5330c8843ab9ec688ff0c3))

## [1.6.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.6.0...v1.6.1) (2024-03-22)


### Bug Fixes

* Table.vue -> added searching and appending, searching bug fixx ([4b5072c](https://gitlab.com/packages2855812/wpmvue-starter/commit/4b5072cc3351bb60acf2f89b90663f4d9361840f))

# [1.6.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.6...v1.6.0) (2024-03-22)


### Bug Fixes

* all components added to registerGlobalComponents ([45a88f9](https://gitlab.com/packages2855812/wpmvue-starter/commit/45a88f9e5617785eb5c9329e904266ef5eff0232))


### Features

* component TopTabs ([1b28fea](https://gitlab.com/packages2855812/wpmvue-starter/commit/1b28fea8455261fecbdd1a726d07fa3c3fff1c78))

## [1.5.6](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.5...v1.5.6) (2024-03-22)


### Bug Fixes

* Table.vue -> added searching and appending, searching bug fixx ([e65e0bd](https://gitlab.com/packages2855812/wpmvue-starter/commit/e65e0bd55e503b04b104998bdc93a9ab793fa8b6))

## [1.5.5](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.4...v1.5.5) (2024-03-22)


### Bug Fixes

* Table.vue -> added searching and appending, searching bug fixx ([81755cc](https://gitlab.com/packages2855812/wpmvue-starter/commit/81755cc0080cfda9e0c74574bf7d72693e7c601c))

## [1.5.4](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.3...v1.5.4) (2024-03-22)


### Bug Fixes

* Table.vue -> added searching and appending ([d51b906](https://gitlab.com/packages2855812/wpmvue-starter/commit/d51b906c18ce2cccff97ee1e929498d4a8031751))

## [1.5.3](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.2...v1.5.3) (2024-03-22)


### Bug Fixes

* carbon constants ([a877a87](https://gitlab.com/packages2855812/wpmvue-starter/commit/a877a8763a05834969090b2dc8782f9f45528c0b))

## [1.5.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.1...v1.5.2) (2024-03-21)


### Bug Fixes

* input error prop ([9ba6287](https://gitlab.com/packages2855812/wpmvue-starter/commit/9ba62873c4da1e3beb70b0f3c8316b1d743603c4))

## [1.5.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.5.0...v1.5.1) (2024-03-21)


### Bug Fixes

* layout, buttons... ([1cdd096](https://gitlab.com/packages2855812/wpmvue-starter/commit/1cdd096e7a66d1a2eecb81d30f2a567e6656d319))

# [1.5.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.4.1...v1.5.0) (2024-03-20)


### Features

* Button component ([b90f9b2](https://gitlab.com/packages2855812/wpmvue-starter/commit/b90f9b25ee46aac0d3069a97fb8591119e4717f7))

## [1.4.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.4.0...v1.4.1) (2024-03-20)


### Bug Fixes

* prepush ([92bfe1f](https://gitlab.com/packages2855812/wpmvue-starter/commit/92bfe1f144949268a64199a637625130aedc1e22))

# [1.4.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.3.2...v1.4.0) (2024-03-20)


### Features

* components Input and Scroll ([823c066](https://gitlab.com/packages2855812/wpmvue-starter/commit/823c066618934bb08b4166f98f8ec6abbbd7c76b))

## [1.3.2](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.3.1...v1.3.2) (2024-03-20)


### Bug Fixes

* prepush ([c76b317](https://gitlab.com/packages2855812/wpmvue-starter/commit/c76b3178810ba3409fb03057508f4cb5f71d2ab5))
* prepush ([faa660a](https://gitlab.com/packages2855812/wpmvue-starter/commit/faa660a836302db19f08cdef109e5b5bb87252c9))

## [1.3.1](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.3.0...v1.3.1) (2024-03-20)


### Bug Fixes

* versioning pacht last update ([a6f45a2](https://gitlab.com/packages2855812/wpmvue-starter/commit/a6f45a26bd70534a32cb00208ae6630a35d90054))

# [1.3.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.2.0...v1.3.0) (2024-03-20)


### Features

* versioning pacht ([7f09ba5](https://gitlab.com/packages2855812/wpmvue-starter/commit/7f09ba59c5f191210eb0467672b3183d282294dd))

# [1.2.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.1.0...v1.2.0) (2024-03-20)


### Features

* versioning pach ([0f12130](https://gitlab.com/packages2855812/wpmvue-starter/commit/0f12130fdbf11a0fe1c163fa8f88a3dd6b2ea9ce))
* versioning pach ([267f189](https://gitlab.com/packages2855812/wpmvue-starter/commit/267f1893c3883f9f076c5d33e3bce37096bff5aa))
* versioning pach ([7d32370](https://gitlab.com/packages2855812/wpmvue-starter/commit/7d32370fa19a142a518ffa6f47a192d202b59f60))

# [1.1.0](https://gitlab.com/packages2855812/wpmvue-starter/compare/v1.0.0...v1.1.0) (2024-03-20)


### Features

* npmrc ([c6f85aa](https://gitlab.com/packages2855812/wpmvue-starter/commit/c6f85aa4ec8d50ce7ddf8eba4762c8877d1827f7))

# 1.0.0 (2024-03-20)


### Features

* readme uipdated ([a9edfa2](https://gitlab.com/packages2855812/wpmvue-starter/commit/a9edfa28c00b7f7c9586e4e84dea2996aac442f5))
