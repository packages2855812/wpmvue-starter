const fs = require('fs');
const path = require('path');
const fse = require('fs-extra');

(async () => {
    // Dynamically import inquirer
    const {default: inquirer} = await import('inquirer');

    // Define your files or modules with identifiable keys
    const scaffolds = {
        pages_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'pages'),
            description: 'Pages folder'
        },
        components_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'components'),
            description: 'Components folder'
        },
        middlewares_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'middlewares'),
            description: 'Middlewares folder'
        },
        utils_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'utils'),
            description: 'Utils folder'
        },
        mixins_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'mixins'),
            description: 'Mixins folder'
        },
        constants_folder: {
            destination: path.join(process.cwd(), 'resources', 'js', 'constants'),
            description: 'Constants folder'
        },
        paginate_mixin: {
            source: path.join(__dirname, 'src','template', 'mixins', 'paginate.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'mixins', "paginate.js"),
            description: 'Paginate mixin'
        },
        router: {
            source: path.join(__dirname, 'src','template', 'routes', 'router.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'routes', "router.js"),
            description: 'Router'
        },
        auth_middleware:{
            source: path.join(__dirname, 'src','template', 'middlewares', 'auth.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'middlewares', "auth.js"),
            description: 'Auth middleware'
        },
        guest_middleware:{
            source: path.join(__dirname, 'src','template', 'middlewares', 'guest.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'middlewares', "guest.js"),
            description: 'Guest middleware'
        },
        store: {
            source: path.join(__dirname, 'src','template', 'store', 'index.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'store', 'index.js'),
            description: 'Vuex store',
        },
        user_module: {
            source: path.join(__dirname, 'src','template', 'store', 'modules', 'user.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'store', 'modules', 'user.js'),
            description: 'User Vuex Module',
        },
        util_global_component_register: {
            source: path.join(__dirname, 'src','template', 'utils', 'registerGlobalComponents.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'utils', 'registerGlobalComponents.js'),
            description: 'Util - register global components',
        },
        util_global_plugins_register: {
            source: path.join(__dirname, 'src','template', 'utils', 'registerGlobalPlugins.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'utils', 'registerGlobalPlugins.js'),
            description: 'Util - register global plugins',
        },
        util_global_mixin_register: {
            source: path.join(__dirname, 'src','template', 'utils', 'registerGlobalMixins.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'utils', 'registerGlobalMixins.js'),
            description: 'Util - register global mixins',
        },
        util_global_variable_define: {
            source: path.join(__dirname, 'src','template', 'utils', 'defineGlobalVariables.js'),
            destination: path.join(process.cwd(), 'resources', 'js', 'utils', 'defineGlobalVariables.js'),
            description: 'Util - register global variables',
        },
        sass_scaff: {
            source: path.join(__dirname, 'src','template', 'sass',),
            destination: path.join(process.cwd(), 'resources', 'sass'),
            description: 'Sass scaffolding',
        },
    };

    // Create folder choices

    // Create file choices
    const fileChoices = Object.entries(scaffolds).map(([key, {description}]) => ({
        name: description,
        value: key
    }));

    try {

        // Ask the user which scaffolds to publish
        const fileAnswers = await inquirer.prompt([
            {
                type: 'checkbox',
                message: 'Select scaffolds to publish:',
                name: 'selectedFiles',
                choices: fileChoices,
                validate: function (answer) {
                    if (answer.length < 1) {
                        return 'You must choose at least one file.';
                    }
                    return true;
                }
            }
        ]);

        // Copy each selected file
        fileAnswers.selectedFiles.forEach(fileKey => {
            const file = scaffolds[fileKey];
            const destinationDir = path.dirname(file.destination);
            if (!fs.existsSync(destinationDir)) {
                fs.mkdirSync(destinationDir, {recursive: true});
            }
            if (fs.existsSync(file.source) && fs.lstatSync(file.source).isDirectory()) {
                // Use fs-extra's copySync to copy the directory
                fse.copySync(file.source, file.destination);
            } else if (fs.existsSync(file.source)) {
                // If it's a file, use the original method
                fs.copyFileSync(file.source, file.destination);
            }
            console.log(`${file.description} published successfully.`);
        });
    } catch (error) {
        console.error('An error occurred:', error);
    }
})();
