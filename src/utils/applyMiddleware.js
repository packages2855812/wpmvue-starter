export function applyMiddleware(router) {
    function nextFactory(context, middleware, index) {
        const subsequentMiddleware = middleware[index];
        // Wrap the original `next` to decide whether to proceed with navigation or call the next middleware
        return (...parameters) => {
            // If parameters are provided, it means we want to navigate/abort immediately
            if (parameters.length > 0) {
                context.next(...parameters);
            } else if (subsequentMiddleware) {
                // Call the next middleware in the chain
                subsequentMiddleware({ ...context, next: nextFactory(context, middleware, index + 1) });
            } else {
                // No more middleware to execute, proceed with the original navigation
                context.next();
            }
        };
    }

    router.beforeEach((to, from, next) => {
        const { middleware } = to.meta;

        if (!middleware) {
            return next();
        }

        const context = { from, next, router, to };
        const middlewareList = Array.isArray(middleware) ? middleware : [middleware];

        if (typeof middlewareList[0] !== 'function') {
            console.warn('Expected first middleware to be a function, but got', middlewareList[0]);
            return next();
        }

        // Start the middleware chain with the first middleware, providing a wrapped `next` function
        const nextMiddleware = nextFactory(context, middlewareList, 1);
        middlewareList[0]({ ...context, next: nextMiddleware });
    });
}
