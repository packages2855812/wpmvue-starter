import {applyDateExtensions} from "../plugins/dateExtensions";

applyDateExtensions();

export function dayDifference(startDate, endDate, zero) {
    if (startDate && endDate) {
        let oneDay = 24 * 60 * 60 * 1000;
        let start = new Date(startDate);
        let end = new Date(endDate);
        return Math.round(
            Math.abs((start.getTime() - end.getTime()) / oneDay)
        );
    }
    return zero ? 0 : 1;
}

export function getDateRangeArray(start, end) {
    let arr = [];
    for (let dt = new Date(start); dt <= new Date(end); dt.setDate(dt.getDate() + 1)) {
        arr.push(new Date(dt));
    }
    return arr;
}

export function validateBirthDate(value) {
    let currentDate = new Date();
    let birthDate = new Date(value);
    let age = currentDate.getFullYear() - birthDate.getFullYear();
    let monthDiff = currentDate.getMonth() - birthDate.getMonth();

    if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())) {
        age--;
    }
    if ((!age && age !== 0) || isNaN(age) || age < 0 || age > 120) {
        return false;
    } else {
        return true;
    }
}

export function dayName(val, locale = "sl-SI") {
    let date = new Date(val);
    return date.toLocaleDateString(locale, {weekday: 'long'});
}

export function today() {
    let date = new Date();
    return (new Date(date)).toISODateTime();
}

export function yesterday() {
    let date = new Date();
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() - 1);
    return (new Date(date)).toISODateTime();
}

export function tomorrow() {
    let date = new Date();
    date.setDate(date.getDate() + 1);
    return (new Date(date)).toISODateTime();
}


export function age(birth_date){
    let ageMS = Date.parse(Date()) - Date.parse(birth_date);
    let age = new Date();
    age.setTime(ageMS);
    return age.getFullYear() - 1970;
}

export function getDaysInMonth(val){
    let year = val.getFullYear();
    let month = val.getMonth();
    let ds = year + "-" + month + "-" + '01' + ' UTC'
    const date = new Date(year, month, 1);

    const dates = [];

    while (date.getMonth() === month) {
        dates.push(new Date(date).toISODate());
        date.setDate(date.getDate() + 1);
    }

    return dates;
}

export function niceDate(date, time) {
    const d = new Date(date);
    const day = d.getDate().toString().padStart(2, '0');
    const month = (d.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-based
    const year = d.getFullYear();
    if (time) {
        const hours = d.getHours().toString().padStart(2, '0');
        const minutes = d.getMinutes().toString().padStart(2, '0');
        return `${day}.${month}.${year} ${hours}:${minutes}`;
    }
    return `${day}.${month}.${year}`;
}

export function shortDate(date, time) {
    const d = new Date(date);
    const day = d.getDate().toString().padStart(2, '0');
    const month = (d.getMonth() + 1).toString().padStart(2, '0');
    if (time) {
        // Using 12-hour format and adding AM/PM
        let hours = d.getHours();
        const minutes = d.getMinutes().toString().padStart(2, '0');
        const ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours.toString().padStart(2, '0') : '12'; // the hour '0' should be '12'
        return `${day}.${month} ${hours}:${minutes} ${ampm}`;
    }
    return `${day}.${month}`;
}

export function subYears(date, years) {
    const result = new Date(date); // Clone the original date to avoid mutating it
    result.setFullYear(result.getFullYear() - years); // Subtract years
    return result;
}

export function addYears(date, years) {
    const result = new Date(date); // Clone the original date to avoid mutating it
    result.setFullYear(result.getFullYear() + years); // Add years
    return result;
}

export function getYear(date){
    const d = new Date(date);
    return d.getFullYear();
}

export function removeSeconds(){

}