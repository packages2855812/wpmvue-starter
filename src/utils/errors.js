export function err_format(errors) {
    return Object.keys(errors).reduce((accumulator, key) => {
        accumulator[key] = errors[key][0];
        return accumulator;
    }, {});
}

export function clear_errors(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        acc[key] = null;
        return acc;
    }, {});
}