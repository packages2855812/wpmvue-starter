export function download(url, name = "filename.pdf") {
    // Logic to download a file
    let link = document.createElement('a');
    link.href = url;

    // Set the download attribute with a filename
    link.setAttribute('download', name); // You can specify a different filename if needed

    // Append the link to the document and trigger the download
    document.body.appendChild(link);
    link.click();

    // Remove the link after triggering download
    document.body.removeChild(link);
}

export function previewPDF() {

}

