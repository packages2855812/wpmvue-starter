export function search(obj, keys, searchTerm) {
    // Split the search term into words and convert to lowercase for case-insensitive comparison.
    const terms = searchTerm.toLowerCase().split(/\s+/);

    // Attempt to find any term within the specified keys of the obj.
    return terms.some(term => keys.some(key => {
        // Try to handle both plain text and JSON strings in properties.
        // If the property is a JSON string, parse it.
        const value = obj[key];
        // If it's an obj or array, convert it to a string to search within.
        if (typeof value === 'object') {
            return JSON.stringify(value).toLowerCase().includes(term);
        }
        if (typeof obj[key] === 'string') {
            return obj[key].toLowerCase().includes(term);
        }
        return false;
    }));
}

export function debounce(func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}