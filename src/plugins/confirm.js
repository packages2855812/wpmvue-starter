// ConfirmPlugin.js
import { createApp } from 'vue';
import Confirm from "../components/modal/Confirm.vue";

export default {
    install(app) {
        const appInstance = createApp(Confirm);
        const mountPoint = document.createElement('div');
        document.body.appendChild(mountPoint);
        const confirmInstance = appInstance.mount(mountPoint);

        app.config.globalProperties.$_confirm = function (message, okBtn,cancelBtn) {
            return confirmInstance.open(message, okBtn, cancelBtn);
        };
    },
};
