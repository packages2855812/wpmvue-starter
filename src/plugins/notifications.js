import Notifications, {notify} from '@kyvg/vue3-notification';

export default {
    install(app) {
        app.use(Notifications)
        app.config.globalProperties._warning = warning; // No $ prefix
        app.config.globalProperties._error = error; // No $ prefix
        app.config.globalProperties._success = success; // No $ prefix
        app.config.globalProperties._handleErrorNotification = handleError; // No $ prefix
    }
};

const warning = (message, duration = 4000) => {
    notify({title: message, type: 'warn', duration: duration}); // Adjust type as necessary
};

const error = (message,duration = 4000) => {
    notify({title: message, type: 'error', duration: duration});
};

const success = (message,duration = 4000) => {
    notify({title: message, type: 'success', duration: duration});
}

const handleError = (err) => {
    if (err.response.data.message) {
        error(err.response.data.message);
    } else {
        error("Prišlo je do napake!", JSON.stringify(err));
    }
};


export {warning, error, success, handleError}