import {printPage} from "../utils/printing";

export default {
    install(app) {
        app.config.globalProperties._printPage = printPage; // No $ prefix
    }
};