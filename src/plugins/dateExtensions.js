export default {
    install(app) {
        applyDateExtensions()
    }
};

export function applyDateExtensions() {
    // Function to generate an array of dates between two dates
    // Extend Date.prototype to include a method for ISO date formatting
    if (!Date.prototype.toISODate) {
        Date.prototype.toISODate = function () {
            return `${this.getFullYear()}-${('0' + (this.getMonth() + 1)).slice(-2)}-${('0' + this.getDate()).slice(-2)}`;
        };
    }

    // Extend Date.prototype to include a method for ISO date-time formatting
    if (!Date.prototype.toISODateTime) {
        Date.prototype.toISODateTime = function () {
            return `${this.toISODate()}T${('0' + this.getHours()).slice(-2)}:${('0' + this.getMinutes()).slice(-2)}:${('0' + this.getSeconds()).slice(-2)}`;
        };
    }

    // Extend Date.prototype to include a method for ISO time formatting
    if (!Date.prototype.toISOTime) {
        Date.prototype.toISOTime = function () {
            return `${('0' + this.getHours()).slice(-2)}:${('0' + this.getMinutes()).slice(-2)}:${('0' + this.getSeconds()).slice(-2)}`;
        };
    }

    // Extend Date.prototype to include a method for ISO time (no seconds) formatting
    if (!Date.prototype.toISOTimeNoSec) {
        Date.prototype.toISOTimeNoSec = function () {
            return `${('0' + this.getHours()).slice(-2)}:${('0' + this.getMinutes()).slice(-2)}`;
        };
    }

    // Extend Date.prototype to include a method for generating a range of dates
    if (!Date.prototype.range) {
        Date.prototype.range = function (secondDate, format = false) {
            let range = [];
            let startDate = new Date(this);
            let endDate = new Date(secondDate);

            while (startDate <= endDate) {
                range.push(format ? startDate.toISODate() : new Date(startDate));
                startDate.setDate(startDate.getDate() + 1);
            }

            return range;
        };
    }

    /**
     * Usage:
     *
     * Import this module early in your application's lifecycle, such as in your main.js file:
     * import './path/to/dateExtensions';
     *
     * Examples:
     * let dates = getDaysArray(new Date("2023-01-01"), new Date("2023-01-05"));
     * console.log(dates); // Array of Date objects between Jan 1 and Jan 5, 2023
     *
     * let isoDate = new Date().toISODate();
     * console.log(isoDate); // Outputs current date in YYYY-MM-DD format
     *
     * let dateRange = new Date("2023-01-01").range(new Date("2023-01-05"), true);
     * console.log(dateRange); // Array of dates in YYYY-MM-DD format between Jan 1 and Jan 5, 2023
     */
}
