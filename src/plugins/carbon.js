import {
    addYears,
    age,
    dayDifference,
    dayName,
    getDateRangeArray, getDaysInMonth, getYear, niceDate, shortDate, subYears,
    today,
    tomorrow,
    validateBirthDate,
    yesterday
} from "../utils/carbon";
import {birth_max, birth_min, hr_format, iso_format} from "../constants/carbonConstants";
import {applyDateExtensions} from "./dateExtensions";
export default {
    install(app) {
        app.config.globalProperties._dayDifference = dayDifference; // No $ prefix
        app.config.globalProperties._getDateRangeArray = getDateRangeArray; // No $ prefix
        app.config.globalProperties._validateBirthDate = validateBirthDate; // No $ prefix
        app.config.globalProperties._dayName = dayName; // No $ prefix
        app.config.globalProperties._today = today; // No $ prefix
        app.config.globalProperties._yesterday = yesterday; // No $ prefix
        app.config.globalProperties._tomorrow = tomorrow; // No $ prefix
        app.config.globalProperties._age = age; // No $ prefix
        app.config.globalProperties._iso_format = iso_format; // No $ prefix
        app.config.globalProperties._hr_format = hr_format; // No $ prefix
        app.config.globalProperties._birth_max = birth_max; // No $ prefix
        app.config.globalProperties._birth_min = birth_min; // No $ prefix
        app.config.globalProperties._niceDate = niceDate; // No $ prefix
        app.config.globalProperties._shortDate = shortDate; // No $ prefix
        app.config.globalProperties._subYears = subYears; // No $ prefix
        app.config.globalProperties._addYears = addYears; // No $ prefix
        app.config.globalProperties._getYear = getYear; // No $ prefix
    }
};