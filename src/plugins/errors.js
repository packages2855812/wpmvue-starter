import {clear_errors, err_format} from "../utils";

export default {
    install(app) {
        app.config.globalProperties._clear_errors = clear_errors; // No $ prefix
        app.config.globalProperties._error = err_format; // No $ prefix
    }
};
