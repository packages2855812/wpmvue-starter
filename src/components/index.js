import Loader from "./Loader.vue";
import Scroll from "./Scroll.vue";
import Section from "./Section.vue";
import Input from "./Input.vue";
import Layout from "./layouts/Layout.vue";
import Card from "./Card.vue";
import Button from "./Button.vue";
import Table from "./Table.vue";
import TopTabs from "./TopTabs.vue";
import ModalWindow from "./modal/ModalWindow.vue";
import Toggle from "./Toggle.vue";
import PopUp from "./PopUp.vue";
import DragDrop from "./DragDrop.vue";


export {
    Section,
    Loader,
    DragDrop,
    Scroll,
    Input,
    Button,
    Layout,
    Card,
    Table,
    TopTabs,
    ModalWindow,
    Toggle,
    PopUp
};