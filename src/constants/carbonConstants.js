

import {yesterday} from "../utils/carbon"
import {subYears} from "../utils/carbon"
export const birth_max = yesterday();
export const birth_min = subYears(yesterday(), 110) ;
export const iso_format = "yyyy-MM-dd";
export const hr_format = "dd.MM.yyyy";
