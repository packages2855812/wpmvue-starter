import {handleError} from "wpm-starter";

export default {
    methods: {
        // handler for Confirm plugin, so default button texts and classes can be changed
        async _confirm(text, okBtn, cancelBtn, btnClass) {
            return await this.$_confirm(text, okBtn, cancelBtn, btnClass);
        },
        async _uploadFile(file, folder, fileName, replace = false) {
            let url = "/api/media/upload";
            let formData = new FormData();
            if (fileName) {
                const newBaseName = fileName.replace(/\//g, "-");
                file = this._renameFile(file, newBaseName);
                // Now you can use newFile, which has the updated name and extension
            }
            formData.append('file', file);
            formData.append('folder', this.folder);
            formData.append('replace', replace);
            try {
                let result = await axios.post(url, formData);
                this._success({text: result.data.message});
                return result.data.data;
            } catch (error) {
                handleError(error);
            }
        },
        async _removeFile(doc) {
            let url = "/api/media/remove";
            let formData = new FormData();

            try {
                let result = await axios.post(url, doc);
                this._success({text: result.data.message});
                return result.data.data;
            } catch (error) {
                handleError(error);
            }
        },
        _renameFile(originalFile, newBaseName) {
            // Extract the file extension from the original file
            const extension = originalFile.name.split('.').pop();

            // Append the extension to the new filename
            const newFileName = `${newBaseName}.${extension}`;

            // Create a new file with the same content but the new name
            return new File([originalFile], newFileName, {
                type: originalFile.type,
                lastModified: originalFile.lastModified,
            });
        },
    }
}