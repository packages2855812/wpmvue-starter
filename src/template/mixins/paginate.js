const TYPING_INTERVAL = 600;
const DEFAULT_PER_PAGE = 15;
export default {
    data() {
        return {
            filter: {
                search: ""
            },
            pagination: {
                per_page: DEFAULT_PER_PAGE,
                current_page: 1,
                page_count: 1,
            },
            list: [],
            appending: false,
            searching: false,
            typingTimer: null,
            query: {
                url: "",
                destination: "",
            },
        }
    },
    methods: {
        handleKeydown() {
            clearTimeout(this.typingTimer);
        },
        handleKeyup() {
            clearTimeout(this.typingTimer);
            this.typingTimer = setTimeout(async () => {
                await this.search(true);
            }, TYPING_INTERVAL);
        },
        handleScroll(e) {
            if (this.appending) return;

            const {target: {scrollTop, scrollHeight, clientHeight}} = e;
            if (Math.ceil(scrollTop + clientHeight) >= (scrollHeight - 10) && this.pagination.current_page < this.pagination.page_count) {
                this.pagination.current_page += 1;
                this.search(false, true);
            }
        },
        async search(searching = false, append = false) {
            if (append)
                this.appending = true;
            if (searching) {
                this.searching = true;
                this.pagination.current_page = 1;
            }
            let data = {
                filter: this.filter,
                per_page: this.pagination.per_page
            };
            axios
                .post(this.query.url + "?page=" + this.pagination.current_page, data)
                .then(result => {
                    if (append) {
                        this[this.query.destination] = [...this[this.query.destination], ...result.data.data.data];
                    } else {
                        this[this.query.destination] = [...result.data.data.data];
                    }

                    this.pagination.page_count = result.data.data.last_page;
                })
                .catch(error => {
                    console.log(error);
                }).finally(() => {
                this.appending = false;
                this.searching = false;
            });
        },

    },
    mounted() {
        this.search(false, true);
        this.$nextTick(function () {
            window.addEventListener('scroll', this.handleScroll);
        });
    },
    beforeUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    },
}
