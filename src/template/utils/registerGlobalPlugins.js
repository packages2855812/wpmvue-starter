import {printing} from "wpm-starter";
import {dateExtensions} from "wpm-starter";
import {carbon} from "wpm-starter";
import {confirm} from "wpm-starter";
export default function registerGlobalPlugins(app) {
    //CLEAN
    app.use(printing)
    app.use(dateExtensions)
    app.use(carbon)
    app.use(confirm)
}
