import {
    Input,
    Loader,
    Scroll,
    Button,
    Card,
    Layout,
    Table,
    TopTabs,
    ModalWindow
} from "wpm-starter";
import Notifications from '@kyvg/vue3-notification'

export default function registerGlobalComponents(app) {
    //CLEAN
    app.use(Notifications)
    app.component('Input', Input);
    app.component('Loader', Loader);
    app.component('Scroll', Scroll);
    app.component('Button', Button);
    app.component('Card', Card);
    app.component('Layout', Layout);
    app.component('Table', Table);
    app.component('TopTabs', TopTabs);
    app.component('Modal', ModalWindow);
}
