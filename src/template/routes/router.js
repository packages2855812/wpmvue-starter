import {createRouter, createWebHistory} from 'vue-router';

//PAGES IMPORT
import {applyMiddleware} from "wpm-starter";




const routes = [
    // ...guest_routes,
    // {
    //     path: '/',
    //     name: "Domov",
    //     redirect:"/nadzorna-plosca",
    //     component: TurismLayout,
    //     children: auth_routes,
    //     meta: {
    //         middleware: [auth_middleware]
    //     }
    // },
];

const router = createRouter({
    routes,
    history: createWebHistory(),
});

//APPLYING MIDDLEWARE LOGIC FOR ROUTER
applyMiddleware(router);

export default router;
