import {createStore} from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import VuexPersistence from "vuex-persist";

// import user from "./modules/user.js";


const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
});

export default createStore({
    modules: {
        // user,
        // dashboard
    },
    plugins: [
        createPersistedState({
            // paths: ['user'] // Only persist the 'user' module, not 'mail'
        })
    ]
});
